import React, { useReducer } from 'react'
import {changeSlide} from './changeSlide'
import './Slider.css'

import {imgList} from '../sliderImg'

const Slider = () => {
    const slides = imgList.map(img => ({...img, active:false}))
    slides[0].active = true;
    
    const [slideList, dispatch] = useReducer(changeSlide, slides)
    const imgSlideElem = slideList.map(({text,src,active}) => {
        const style = active ? {display: 'block'} : {display: 'none'}
        return (
            <div className="" style={style}> <img src={src} />
            <p>{text}</p></div>
        )
    });

    return (
        <div className='sliderWrapper'>
           <button className='btn' onClick={() => dispatch({type: 'MOVE_LEFT'})}> left </button>
            <div>{imgSlideElem}</div>
            <button className='btn' onClick={() => dispatch({type: 'MOVE_RIGHT'})}> right </button>
        </div>
    );
} 
    
export default Slider;       